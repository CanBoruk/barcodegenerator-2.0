package constants;

import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.DeviceRgb;


public interface Cnsts {
    // All Fields in Interface are "public static final"
    Color BLACK = new DeviceRgb(10, 10, 10); // Schwarz
    Color DARKGREY = new DeviceRgb(68, 68, 68); // DarkGrey
    Color WHITE = new DeviceRgb(255, 255, 255); // White
    Color PINK = new DeviceRgb(255, 153, 204); // Pink
    Color SILVER = new DeviceRgb(230, 230, 255); // Silver
    Color GREY = new DeviceRgb(204, 204, 204); // Grey
    Color RED = new DeviceRgb(197, 0, 11); // Red
    Color BLUE = new DeviceRgb(0, 132, 209); // Blue
    Color BROWNISH = new DeviceRgb(230, 163, 69); // Brownish
}
