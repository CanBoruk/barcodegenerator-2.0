package ui;

import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Table;
import constants.Cnsts;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import pdf.PDF;
import pdf.PDFinformation;


import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.concurrent.ExecutionException;

public class UiController implements Initializable, Cnsts {
    @FXML
    private AnchorPane anchorID;

    public Button deleteButton;
    public void deleteHovored(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("Barcode von der Liste entfernen");
        deleteButton.setTooltip(tooltip);
    }
    @FXML
    void clickDelete(ActionEvent event) {
        delteItem();
    }

    public Button createButton;
    public void createHovored(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("PDF mit Barcodes erstellen");
        createButton.setTooltip(tooltip);
    }
    @FXML
    void clickCreate(ActionEvent event) { createpage(); }

    public Button cancelButton;
    public void cancelHovered(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("Auswahl leeren");
        cancelButton.setTooltip(tooltip);
    }
    @FXML
    void clickCancel(ActionEvent event) {
        clearValues();
    }

    public Button addButton;
    public void addHovered(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("Barocde zur Liste hinzufügen");
        addButton.setTooltip(tooltip);
    }
    @FXML
    void clickAdd(ActionEvent event) {
        addItem();
    }

    @FXML
    public ListView<PDFinformation> lv_ausgabe;
    public void lv_Hovored(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("Barcodes, die erstellt werden.");
        lv_ausgabe.setTooltip(tooltip);
    }
    @FXML
    private TextField code;
    public void codeHovered(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("Barcode");
        code.setTooltip(tooltip);
    }

    @FXML
    private TextField line2;
    public void line2Hovered(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("Zeile 1.");
        line2.setTooltip(tooltip);
    }

    @FXML
    private TextField line3;
    public void line3Hovered(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("Zeile 2.");
        line3.setTooltip(tooltip);
    }

    @FXML
    private ChoiceBox<PDFinformation.backgroundcolor> background;
    public void colorHovered(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("Farbe");
        background.setTooltip(tooltip);
    }

    @FXML
    private TextField amount;
    public void amountHovered(MouseEvent mouseEvent) {
        final Tooltip tooltip = new Tooltip();
        tooltip.setText("Anzahl der Barcodes");
        amount.setTooltip(tooltip);
    }

    @FXML
    public void lv_clicked(javafx.scene.input.MouseEvent mouseEvent) {
        selectLV();
    }

    public void initialize(URL url, ResourceBundle resourceBundle) {
        background.getItems().setAll(PDFinformation.backgroundcolor.values()); // assign enum to choiceBox
        background.getSelectionModel().selectFirst();
    }

    public void clearValues() {
        code.clear();
        line2.clear();
        line3.clear();
        background.getSelectionModel().selectFirst();
        amount.clear();
    }

    public void addItem() {

        try {
            if(code.getText().isEmpty() || line2.getText().isEmpty() || line3.getText().isEmpty() || amount.getText().isEmpty()){
                throw new NullPointerException();
            }
            lv_ausgabe.getItems().add(new PDFinformation(code.getText(), line2.getText(), line3.getText(), background.getSelectionModel().getSelectedItem().toString(), Integer.parseInt(amount.getText())));

        }catch(NullPointerException e){
            alertWindow(Alert.AlertType.ERROR,"Fehler", "Leeres Feld!", "Ein oder mehrere Felder sind leer");
        }
        catch (NumberFormatException e) {
            alertWindow(Alert.AlertType.ERROR,"Fehler", "Anzahl ist falsch!", "Sie dürfen nur Zahlen in das Feld \"Anzahl\" eintragen");
        }
    }

    public void delteItem() {
        try {
            lv_ausgabe.getItems().remove(lv_ausgabe.getSelectionModel().getSelectedIndex());
            PDFinformation.amount--;
        } catch (Exception e) {
            alertWindow(Alert.AlertType.ERROR,"Fehler", "Nichts ausgewählt!", "Sie müssen mindestens einen Barcode auswählen.");
        }
    }

    public void selectLV() {
        try {
            PDFinformation printer_safed = lv_ausgabe.getSelectionModel().getSelectedItem();
            code.setText(printer_safed.getCode());
            line2.setText(printer_safed.getLine2());
            line3.setText(printer_safed.getLine3());
            background.getSelectionModel().select(PDFinformation.backgroundcolor.VALUE4);
            background.setValue(switchColor(printer_safed.getBackground()));
            amount.setText(String.valueOf(printer_safed.getNumber()));
            //System.out.println(printer_safed); //ListView clicked output
        }catch(Exception e){
            System.out.println("nichts ausgewählt");
        }
    }

    public void createpage() {

        File file = safeDialog();

        try {
            PdfDocument doc = PDF.openPdf(file);
            Document document = PDF.createDocument(doc);
            Table table = PDF.createTable(doc);

            for (int i = 0; i < PDFinformation.amount; i++) {
                PDFinformation values;
                Color print = null;
                lv_ausgabe.getSelectionModel().selectIndices(i);
                values = lv_ausgabe.getSelectionModel().getSelectedItem();

                switch (values.getBackground()) {
                    case "Dunkelgrau":
                        print = DARKGREY;
                        break;
                    case "Schwarz":
                        print = BLACK;
                        break;
                    case "Rosa":
                        print = PINK;
                        break;
                    case "Silber":
                        print = SILVER;
                        break;
                    case "Grau":
                        print = GREY;
                        break;
                    case "Rot":
                        print = RED;
                        break;
                    case "Blau":
                        print = BLUE;
                        break;
                    case "Bräunlich":
                        print = BROWNISH;
                        break;
                }
                table = PDF.createCell(doc, table, values.getCode(), values.getLine2(), values.getLine3(), print, values.getNumber());
            }
            PDF.closeDoc(document, table);

        } catch (Exception e) {
            System.out.println("abgebrochen");
            //e.printStackTrace(); // Full error code
        }

    }

    public File safeDialog() {
        final FileChooser fileChooser = new FileChooser();
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter("PDF", "*.pdf")); // Window Configuration
        fileChooser.setTitle("Speichern unter");
        Stage stage = (Stage) anchorID.getScene().getWindow();
        File file = fileChooser.showSaveDialog(stage);
        return file;
    }

    private PDFinformation.backgroundcolor switchColor(String color) {
        PDFinformation.backgroundcolor switchedColor = null;
        switch (color) {
            case "Weiß":
                switchedColor = PDFinformation.backgroundcolor.VALUE1;
                break;
            case "Dunkelgrau":
                switchedColor = PDFinformation.backgroundcolor.VALUE2;
                break;
            case "Schwarz":
                switchedColor = PDFinformation.backgroundcolor.VALUE3;
                break;
            case "Rosa":
                switchedColor = PDFinformation.backgroundcolor.VALUE4;
                break;
            case "Silber":
                switchedColor = PDFinformation.backgroundcolor.VALUE5;
                break;
            case "Grau":
                switchedColor = PDFinformation.backgroundcolor.VALUE6;
                break;
            case "Rot":
                switchedColor = PDFinformation.backgroundcolor.VALUE7;
                break;
            case "Blau":
                switchedColor = PDFinformation.backgroundcolor.VALUE8;
                break;
            case "Bräunlich":
                switchedColor = PDFinformation.backgroundcolor.VALUE9;
                break;
        }
        return switchedColor;
    }

    private Alert alertWindow(Alert.AlertType aType, String titleTxt, String shortInformation, String information){
        Alert alert = new Alert(aType);
        alert.setTitle(titleTxt);
        alert.setHeaderText(shortInformation);
        alert.setContentText(information);
        alert.showAndWait();
        return null;
    }
}
