package pdf;

import constants.Cnsts;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import org.apache.log4j.BasicConfigurator;
import java.io.File;

public class Main extends Application implements Cnsts {

    @Override
    public void start(Stage primaryStage) throws Exception{
        Parent root = FXMLLoader.load(getClass().getResource("/UI.fxml"));
        primaryStage.setScene(new Scene(root));
        primaryStage.setTitle("Barcode Generator");
        final File f = new File(Application.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        System.out.println(f);
        primaryStage.getIcons().add(new Image( "/Barcode.png"));
        primaryStage.setResizable(false);
        primaryStage.show();
        }

    public static void main(String[] args)  {
        BasicConfigurator.configure();
        launch(args);
    }

}
