package pdf;

import constants.Cnsts;

public class PDFinformation implements Cnsts {
    public static int amount = 0;
    private String code = null;
    private String line2 = null;
    private String line3 = null;
    private String background = null;

    public String getCode() {
        return code;
    }

    public String getLine2() {
        return line2;
    }

    public String getLine3() {
        return line3;
    }

    public String getBackground() {
        return background;
    }

    public int getNumber() {
        return number;
    }

    private int number = 0;

    public PDFinformation() {
        amount++;
    }

    @Override
    public String toString() {
        return   "Barcode: " + code  +
                ", Zeile 2: " + line2  +
                ", Zeile 3: " + line3  +
                ", Hintergrund: " + background  +
                ", Anzahl: " + number ;
    }

    public PDFinformation(String code, String line2, String line3, String background, int number) { //Constructor
        this();
        this.code = code;
        this.line2 = line2;
        this.line3 = line3;
        this.background = background;
        this.number = number;
    }

    public enum backgroundcolor {
        VALUE1("Weiß"),
        VALUE2("Dunkelgrau"),
        VALUE3("Schwarz"),
        VALUE4("Rosa"),
        VALUE5("Silber"),
        VALUE6("Grau"),
        VALUE7("Rot"),
        VALUE8("Blau"),
        VALUE9("Bräunlich");

        private String value;

        backgroundcolor(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        @Override
        public String toString() {
            return this.value;
        }
    }
}
