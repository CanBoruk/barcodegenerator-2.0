package pdf;

import com.itextpdf.barcodes.Barcode128;
import com.itextpdf.kernel.colors.Color;
import com.itextpdf.kernel.colors.ColorConstants;
import com.itextpdf.kernel.colors.DeviceRgb;
import com.itextpdf.kernel.geom.PageSize;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.borders.Border;
import com.itextpdf.layout.borders.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;
import com.itextpdf.layout.property.HorizontalAlignment;
import com.itextpdf.layout.property.TextAlignment;
import com.itextpdf.layout.property.UnitValue;
import constants.Cnsts;

import java.io.File;

public class PDF implements Cnsts {

    public static Document createDocument(PdfDocument pdfDoc) {
        Document document = new Document(pdfDoc, new PageSize(PageSize.A4));
        document.setLeftMargin(0);
        document.setTopMargin(25);
        return document;
    }

    public static PdfDocument openPdf(File filename) throws Exception {
        PdfDocument pdfDoc = new PdfDocument(new PdfWriter(filename));
        return pdfDoc;
    }

    public static void closeDoc(Document document, Table table) {
        document.add(table);
        document.close();
    }

    public static Table createTable(PdfDocument pdfDoc) {

        Table table = new Table(UnitValue.createPercentArray(2)).useAllAvailableWidth();
        table.setWidth(pdfDoc.getDefaultPageSize().getWidth());
        return table;
    }

    public static Table createCell(PdfDocument pdfDoc, Table table, String code, String line2, String line3, Color background, int number) {

        for(int i = 0; i < number; i++ ) {
            table.addCell(createText(code, line2, line3, background, pdfDoc));
        }
        return table;

    }

    public static Table createBarcode(String code, PdfDocument pdfDoc) {
        Barcode128 barcode = new Barcode128(pdfDoc);
        barcode.setCodeType(Barcode128.CODE128);
        barcode.setCode(code);
        barcode.setFont(null);
        Table table = new Table(1);
        Cell cell = new Cell().add(new Image(barcode.createFormXObject(null, null, pdfDoc)).
                setHorizontalAlignment(HorizontalAlignment.CENTER).
                scaleAbsolute(150, 50).
                setPaddingLeft(100)).
                setPadding(5).
                setBackgroundColor(ColorConstants.WHITE)
                .setBorder(Border.NO_BORDER);
        table.setHorizontalAlignment(HorizontalAlignment.CENTER);
        table.addCell(cell);
        return table;
    }

    public static Cell createText(String code, String line2, String line3, Color background, PdfDocument pdfDoc) {
        Cell cell = new Cell();
        cell.add(createBarcode(code, pdfDoc));
        //cell.setHeight(105); //height 3,7cm
        cell.setPaddingTop(5);
        Paragraph l1 = new Paragraph(code);
        Paragraph l2 = new Paragraph(line2);
        Paragraph l3 = new Paragraph(line3);
        l1.setFontSize(8);
        l2.setFontSize(11);
        l3.setFontSize(9);
        l1.setTextAlignment(TextAlignment.CENTER);
        l2.setTextAlignment(TextAlignment.CENTER);
        l3.setTextAlignment(TextAlignment.CENTER);
        cell.add(l1);
        cell.add(l2);
        cell.add(l3);
        cell.setBackgroundColor(background);
        if (background == BLACK || background == DARKGREY){
            cell.setFontColor(WHITE);
            cell.setBorder(new SolidBorder(Color.convertRgbToCmyk(new DeviceRgb(255, 255, 255)),(float)0.5));

        }
        return cell;
    }
}



