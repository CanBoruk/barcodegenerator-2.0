module pdf {
    requires kernel;
    requires javafx.graphics;
    requires javafx.fxml;
    requires log4j;
    requires barcodes;
    requires layout;
    requires javafx.controls;

    opens pdf to javafx.fxml;
    opens ui;

    exports pdf;
    exports ui;


}